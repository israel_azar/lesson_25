﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class RoundTable<T> : IEnumerable<T> where T : INameable
    {
        private List<T> _entities = new List<T>();

        public void Add (T entity)
        {
            _entities.Add(entity);
        }
        public void RemoveAt (int number)
        {
            if (number < _entities.Count)
            {
                _entities.RemoveAt(number);
            }
            else if (number >= _entities.Count)
            {
                _entities.RemoveAt(number % _entities.Count);
            }
        }
        public void Clear()
        {
            _entities.Clear();
        }
        public void InsertAt (int number , T entity)
        {
            if (number < _entities.Count)
            {
                _entities.Insert(number, entity);
            }
            else if (number >= _entities.Count)
            {
                _entities.Insert(number % _entities.Count, entity);
            }
        }
        public void Sort()
        {

        }
        public List<T> GetRounded (int number)
        {
            List<T> extra_list = new List<T>(number);
            int counter = 0;
            if (number < _entities.Count)
            {
                for (int i = 0; i < number; i++)
                {
                    extra_list.Add(_entities[number]);
                }
            }
            else if (number >= _entities.Count)
            {
                for (int i = 0; i < _entities.Count * number; i++)
                {
                    if (i == _entities.Count)
                    {
                        i = 0;
                    }
                    extra_list.Add(_entities[i]);
                    counter++;
                    if (counter > number)
                    {
                        break;
                    }
                }
            }
            return extra_list;
        }
        public T this [int index]
        {
            get
            {
                if (index < _entities.Count)
                {
                    return _entities[index];
                }
                else
                {
                    return _entities[index % _entities.Count];
                }
            }
        }
        public T this [string name]
        {
            get
            {
                T entity = default;
                for (int i = 0; i < _entities.Count; i++)
                {
                    if (name == _entities[i].Name)
                    {
                        entity = _entities[i];
                        break;
                    }
                    else
                    {
                        
                    }
                }
                return entity;
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return _entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _entities.GetEnumerator();
        }

        public override string ToString()
        {
            for (int i = 0; i < _entities.Count; i++)
            {
                Console.WriteLine(_entities[i]);
            }
            return "";
        }
    }
}
