﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Magician : INameable
    {
        public string Name { get; private set; }
        public string Birth_Town { get; private set; }
        public string Favorite_Spell { get; private set; }

        public Magician(string name, string birth_Town, string favorite_Spell)
        {
            Name = name;
            Birth_Town = birth_Town;
            Favorite_Spell = favorite_Spell;
        }

        public string this[string field]
        {
            get
            {
                if (field == "Name")
                {
                    return Name;
                }
                if (field == "Birth Town")
                {
                    return Birth_Town;
                }
                if (field == "Favorite Spell")
                {
                    return Favorite_Spell;
                }
                else
                {
                    return "Unknown";
                }
            }
            set
            {
                if (field == "Name")
                {
                    Name = value;
                }
                if (field == "Birth Town")
                {
                    Birth_Town = value;
                }
                if (field == "Favorite Spell")
                {
                    Favorite_Spell = value;
                }
            }
        }

        public override string ToString()
        {
            return "Name: " + Name + " Birth Town: " + Birth_Town + " Favorite Spell: " + Favorite_Spell;
        }
    }
}
