﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Program
    {
        static void Main(string[] args)
        {
            Magician israel = new Magician("Israel", "Kiryat Malachi", "Abra Cadabra");
            Magician lior = new Magician("Lior", "Ramla", "Hokus Pokus");

            Knight naor = new Knight("Naor", "Kiryat Malachi", "Champ");
            Knight yarin = new Knight("Yarin", "Sderot", "Smart");

            RoundTable <Magician> best_table = new RoundTable<Magician>();
            best_table.Add(israel);
            best_table.Add(lior);
            Console.WriteLine(best_table);
            //best_table.RemoveAt(3);
            //Console.WriteLine(best_table);
            Magician netanel = new Magician("Netanel", "Kiryat Malachi", "Gone");
            best_table.InsertAt(2,netanel);
            Console.WriteLine(best_table);
            //Console.WriteLine(best_table.GetRounded(30));
            Console.WriteLine(best_table[30]);
            Console.WriteLine(best_table["Israel"]);
        }
    }
}
