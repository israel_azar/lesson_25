﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Knight : INameable
    {
        public string Name { get; private set; }
        public string Birth_Town { get; private set; }
        public string Title { get; private set; }

        public Knight(string name, string birth_Town, string title)
        {
            Name = name;
            Birth_Town = birth_Town;
            Title = title;
        }

        public string this [string field]
        {
            get
            {
                if (field == "Name")
                {
                    return Name;
                }
                if (field == "Birth Town")
                {
                    return Birth_Town;
                }
                if (field == "Title")
                {
                    return Title;
                }
                else
                {
                    return "Unknown";
                }
            }
            set
            {
                if (field == "Name")
                {
                    Name = value;
                }
                if (field == "Birth Town")
                {
                    Birth_Town = value;
                }
                if (field == "Title")
                {
                    Title = value;
                }
            }
        }

        public override string ToString()
        {
            return "Name: " + Name + " Birth Town: " + Birth_Town + " Title: " + Title;
        }
    }
}
